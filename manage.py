#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__)))
    PROJECT_ROOT = os.path.abspath(os.path.join(PROJECT_PATH, 'Aplikacje'))
    sys.path.insert(0, os.path.join(PROJECT_ROOT, 'lib'))
    

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Aplikacje.settings.base")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
