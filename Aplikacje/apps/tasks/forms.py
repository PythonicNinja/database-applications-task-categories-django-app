# -*- coding: utf-8 -*-
__author__ = 'wojtek'
from django import forms

from django.forms import ModelForm, models, fields, Form

from tasks.models import Tasks

class TasksForm(forms.ModelForm):
    def __init__(self,*args, **kwargs):
        id_kat = kwargs.pop('id_kat',None)
        super(TasksForm, self).__init__(*args, **kwargs)
        self.fields['trudnosc'].widget.attrs['class'] = 'hide'
        if id_kat!=None:
            self.fields['id_kat'].initial = id_kat

    class Meta:
        model = Tasks