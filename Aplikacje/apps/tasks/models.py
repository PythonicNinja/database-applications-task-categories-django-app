from django.db import models

# Create your models here.
class Tasks(models.Model):
    id_kat = models.ForeignKey('category.Category', verbose_name='Nazwa nad kategorii', blank=True, null=True)
    nazwa = models.CharField(max_length=155, verbose_name='Nazwa')
    plik_pdf = models.FileField(verbose_name='Plik', upload_to='pliki_pdf/%Y/%m/%d')
    trudnosc = models.IntegerField(verbose_name='Trudnosc', max_length=1)
