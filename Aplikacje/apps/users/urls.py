# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^login/$','users.views.login',name='login_user'),
    url(r'^logout/$','users.views.logout',name='logout_user'),
)



