# -*- coding: utf-8 -*-
import json
from datetime import timedelta

from django.db.models import Q
from django.conf import settings
from django.contrib import messages
from django.template.loader import get_template
from django.contrib.auth.models import Permission
from django.utils.translation import ugettext as _
from django.core.mail import EmailMultiAlternatives
from django.template import Context, RequestContext
from django.contrib.auth import login as django_login
from django.template.response import TemplateResponse
from django.contrib.auth import logout as django_logout
from django.contrib.auth import authenticate as django_authenticate
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.contrib.auth.decorators import permission_required, login_required

from users.forms import LoginForm
from tasks.models import Tasks

def login(request):

    imgs = Tasks.objects.filter(Q(plik_pdf__icontains=".jpg") | Q(plik_pdf__icontains=".png"))
    context = {
        'imgs': imgs
    }
    username = password = ''
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('user')
            password = form.cleaned_data.get('password')
            user = django_authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    if not user.is_staff and abs(user.last_login - user.date_joined) < timedelta(seconds=2) :
                        django_login(request, user)
                        return HttpResponseRedirect("/user/password/change/")
                    else:
                        django_login(request, user)
                        return HttpResponseRedirect("/")
                else:
                    messages.error(request, _('Your account has been blocked. Contact the administrator.'))
            else:
                messages.error(request, _('The user and / or password is invalid'))
        else:
            messages.error(request, _('The user and / or password is invalid'))
        context.update({
            'form': form
        })
        return TemplateResponse(request, "users/login.html", context)
    else:
        form = LoginForm()
        context.update({
            'form': form,
            'username': username
        })
        return TemplateResponse(request, "users/login.html",context)

@login_required
def logout(request):

    django_logout(request)
    messages.success(request, _('You have successfully logged out'))
    return redirect("/user/login/")
