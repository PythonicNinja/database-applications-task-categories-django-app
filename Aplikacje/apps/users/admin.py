# -*- coding: utf-8 -*-
__author__ = 'wojtek'

from django.contrib import admin

from users.models import CUser

class CUserAdmin(admin.ModelAdmin):
    pass

admin.site.register(CUser, CUserAdmin)

