# -*- coding: utf-8 -*-
__author__ = 'wojtek'

from django import forms
from django.utils.translation import ugettext_lazy as _


confirm_password_err = {
    'required': 'To pole jest wymagane!',
    'invalid': u'Hasła nie są takie same'
}

class LoginForm(forms.Form):
    user = forms.CharField(label = _("Username"), required=True)
    password = forms.CharField(label = _("Password"), widget=forms.PasswordInput, required=True)
