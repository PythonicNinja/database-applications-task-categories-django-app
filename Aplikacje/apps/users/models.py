# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser



class CUser(AbstractUser):

    def __unicode__(self):
        return u'%s' % (self.username)