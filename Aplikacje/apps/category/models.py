from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from django.db import models
# Create your models here.



class Category(MPTTModel):
    nazwa=models.CharField(max_length=50, null=False, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', verbose_name='Nad Kategoria')

    def __unicode__(self):
        if self.parent==None:
            return "%s" %(self.nazwa)
        else:
            return "%s -> %s" %(self.nazwa,self.parent)

    class MPTTMeta:
        order_insertion_by = ['nazwa']