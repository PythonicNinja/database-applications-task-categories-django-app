from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static


urlpatterns = patterns('',
    url(r'^$', 'category.views.list_main_categories',name='list_main_categories'),
    url(r'^(?P<id>\d+)/$', 'category.views.list_sub_categories', name='list_sub_categories'),
    url(r'^category/(?P<id>\d+)/edit/$', 'category.views.edit_category', name='edit_category'),
    url(r'^category/(?P<id>\d+)/delete/$', 'category.views.delete_category', name='delete_category'),
    url(r'^task/(?P<id>\d+)/edit/$', 'category.views.edit_task', name='edit_task'),
    url(r'^task/(?P<id>\d+)/delete/$', 'category.views.delete_task', name='delete_task'),
)
