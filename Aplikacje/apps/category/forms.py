# -*- coding: utf-8 -*-
__author__ = 'wojtek'
from django import forms

from django.forms import ModelForm, models, fields, Form

from category.models import Category

class CategoryForm(forms.ModelForm):

    def clean_nazwa(self):
        value = self.cleaned_data['nazwa']
        if value == u'Mati':
            raise forms.ValidationError('Matiego nie chcemy!!!')
        return value

    def __init__(self,*args, **kwargs):
        parent = kwargs.pop('parent',None)
        super(CategoryForm, self).__init__(*args, **kwargs)
        if parent!=None:
            self.fields['parent'].initial = parent

    class Meta:
        model = Category