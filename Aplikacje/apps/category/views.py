from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse

from category.forms import CategoryForm
from category.models import Category
from tasks.forms import TasksForm
from tasks.models import Tasks


def list_main_categories(request):
    context={
        'categories': Category.objects.filter(parent=None),
        'tasks': Tasks.objects.filter(id_kat=None),
    }
    if request.POST:
        print request.POST
        if len([x for x in request.POST.keys() if 'task' in x])>0:
            taskForm = TasksForm(request.POST, request.FILES, prefix='task')
            if taskForm.is_valid():
                taskForm.save(commit=True)
            form = CategoryForm(prefix='category')

        if len([x for x in request.POST.keys() if 'category' in x])>0:
            form = CategoryForm(request.POST, prefix='category')
            if form.is_valid():
                form.save(commit=True)
            taskForm = TasksForm(prefix='task')

    else:
        form = CategoryForm(prefix='category')
        taskForm = TasksForm(prefix='task')
    context.update({
        'form': form,
        'taskForm': taskForm
    })
    return TemplateResponse(request,"index.html",context)

def list_sub_categories(request, id):

    parent = get_object_or_404(Category, pk = id)
    kwargs = {'parent': parent}
    form = CategoryForm(prefix='category', **kwargs)
    kwargs = {'id_kat': parent}
    taskForm = TasksForm(prefix='task', **kwargs)

    context={
        'categories': Category.objects.filter(parent=id),
        'tasks': Tasks.objects.filter(id_kat=id),
        'form': form,
        'taskForm': taskForm
    }

    return TemplateResponse(request,"index.html",context)

@login_required
def edit_category(request, id):
    category = get_object_or_404(Category,pk=id)

    if request.POST:
        form = CategoryForm(request.POST, instance=category)
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect(reverse('list_main_categories'))
        else:
            context = {
                'category': category,
                'form': form
            }
    else:
        form = CategoryForm(instance=category)

        context = {
            'category': category,
            'form': form,
        }
    return TemplateResponse(request,"edit.html",context)

@login_required
def edit_task(request, id):
    task = get_object_or_404(Tasks,pk=id)

    if request.POST:
        form = TasksForm(request.POST, request.FILES, instance=task)
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect(reverse('list_main_categories'))
        else:
            context = {
                'task': task,
                'form': form
            }
    else:
        form = TasksForm(instance=task)

        context = {
            'task': task,
            'form': form,
        }
    return TemplateResponse(request,"edit_task.html",context)

@login_required
def delete_task(request, id):
    task = get_object_or_404(Tasks,pk=id)
    task.delete()
    return redirect(request.META['HTTP_REFERER'])


@login_required
def delete_category(request, id):
    category = get_object_or_404(Category,pk=id)
    category.delete()
    return redirect(request.META['HTTP_REFERER'])
