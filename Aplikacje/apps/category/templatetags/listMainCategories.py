# -*- coding: utf-8 -*-
__author__ = 'wojtek'

from django import template

from category.models import Category

register = template.Library()

@register.inclusion_tag('main_categories.html')
def listMainCategories(request):
    main_categories = Category.objects.filter(parent=None)
    nodes = Category.objects.all()
    return {
        'request': request,
        'nodes': nodes,
        'main_categories': main_categories
    }


@register.inclusion_tag('categories_tree.html')
def categories(request):
    nodes = Category.objects.all()
    return {
        'request': request,
        'nodes': nodes,
    }


@register.inclusion_tag('main_categories_ul.html')
def listMainCategoriesUl(request):
    main_categories = Category.objects.filter(parent=None)
    return {
        'request': request,
        'main_categories': main_categories
    }