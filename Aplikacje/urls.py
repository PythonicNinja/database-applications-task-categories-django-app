from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('category.urls')),
    url(r'^user/', include('users.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
